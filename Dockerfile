FROM python:3.9-slim
ENV PYTHONUNBUFFERED=1
COPY . /code/
WORKDIR /code
RUN pip install -r requirements.txt